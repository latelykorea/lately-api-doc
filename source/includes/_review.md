# Review

## Retrieves reviews for specific product

```shell
curl "lately.co.kr/rest/V1/products/reviews/WJ01?pageSize=10&currentPage=1" \
  -H "Content-Type: application/json"
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
{
  "reviews_count": 3,
  "rating_summary": 60,
  "reviews": [
    {
      "review_id": 54,
      "created_at": "2016-04-18 11:58:06",
      "product_id": 1,
      "title": "title",
      "detail": "detail",
      "nickname": "nickname",
      "customer_id": 1,
      "rating": 5
    },
    {
      "review_id": 53,
      "created_at": "2016-04-18 11:55:59",
      "product_id": 1,
      "title": "title",
      "detail": "detail",
      "nickname": "nickname",
      "customer_id": 2,
      "rating": 3
    },
    {
      "review_id": 52,
      "created_at": "2016-04-18 11:55:47",
      "product_id": 1,
      "title": "title",
      "detail": "detail",
      "nickname": "nickname",
      "customer_id": 3,
      "rating": 1
    }
  ]
}
```

특정 제품(`:sku`)에 대한 리뷰들을 한 페이지씩 가져옵니다.

### HTTP Request

`GET rest/V1/products/reviews/:sku`

### Response

JSON [Product Reviews](#product-reviews)

### Query Parameters
Parameter | Default | Description
--------- | ------- | -----------
pageSize |  | 한번에 가져올 페이지의 크기
currentPage |  | 조회할 페이지


## Get my review for specific product

```shell
curl "lately.co.kr/rest/V1/products/reviews/:sku/mine" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer meowmeowmeow"
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
{
  "review_id": 54,
  "created_at": "2016-04-18 11:58:06",
  "product_id": 1,
  "title": "title",
  "detail": "detail",
  "nickname": "nickname",
  "customer_id": 1,
  "rating": 5
}
```

특정 제품(`:sku`)에서 해당 사용자가 쓴 리뷰를 가져옵니다.

### HTTP Request

`GET rest/V1/products/reviews/:sku/mine`

### Response

JSON [Review Item](#review-item)


## Write a review

```shell
curl -X POST "lately.co.kr/rest/V1/products/reviews/WJ01" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer meowmeowmeow" \
  -d '{
    "nickname": "reviewer nickname",
    "title": "review title",
    "detail": "whatever what you want to leave a review",
    "rating": 2
  }'
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
{
  "review_id": 54,
  "created_at": "2016-04-18 11:58:06",
  "product_id": 1,
  "title": "review title",
  "detail": "whatever what you want to leave a review",
  "nickname": "reviewer nickname",
  "customer_id": 1,
  "rating": 2
}
```

특정 제품(`:sku`)에 대한 리뷰를 작성합니다.

### HTTP Request

`POST rest/V1/products/reviews/:sku`

### Response

JSON [Review Item](#review-item)

### Payload

Name | Type | Description
-----|------|------------
nickname | string | 리뷰를 등록한 닉네임
title | string | 리뷰의 제목
detail | string | 리뷰의 내용
rating | int | 리뷰에 포함될 평점


## Modify a review

```shell
curl -X PUT "lately.co.kr/rest/V1/reviews/54" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer meowmeowmeow" \
  -d '{
    "nickname": "reviewer nickname",
    "rating": 2
  }'
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
{
  "review_id": 54,
  "created_at": "2016-04-18 11:58:06",
  "product_id": 1,
  "title": "review title",
  "detail": "whatever what you want to leave a review",
  "nickname": "reviewer nickname",
  "customer_id": 1,
  "rating": 2
}
```

특정 리뷰(`:review_id`)를 수정합니다.

### HTTP Request

`PUT rest/V1/reviews/:review_id`

### Response

JSON [Review Item](#review-item)

### Payload

Name | Type | Description
-----|------|------------
nickname | string or null | 리뷰를 등록한 닉네임
title | string or null | 리뷰의 제목
detail | string or null | 리뷰의 내용
rating | int or null | 리뷰에 포함될 평점


## Remove a review

```shell
curl -X DELETE "lately.co.kr/rest/V1/reviews/54" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer meowmeowmeow"
```

> 위의 명령어는 리턴하는 값이 없습니다.

특정 리뷰(`:review_id`)를 삭제합니다.

### HTTP Request

`PUT rest/V1/reviews/:review_id`

### Response

리턴하는 값 없이 성공(200)을 리턴합니다.


## Retrieves my whole reviews

```shell
curl "lately.co.kr/rest/V1/reviews/mine" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer meowmeowmeow"
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
[
  {
    "review_id": 54,
    "created_at": "2016-04-18 11:58:06",
    "product_id": 1,
    "title": "title",
    "detail": "detail",
    "nickname": "nickname",
    "customer_id": 1,
    "rating": 1
  },
  {
    "review_id": 53,
    "created_at": "2016-04-18 11:55:59",
    "product_id": 2,
    "title": "title",
    "detail": "detail",
    "nickname": "nickname",
    "customer_id": 1,
    "rating": 2
  },
  {
    "review_id": 52,
    "created_at": "2016-04-18 11:55:47",
    "product_id": 3,
    "title": "title",
    "detail": "detail",
    "nickname": "nickname",
    "customer_id": 1,
    "rating": 3
  }
]
```

사용자가 작성한 리뷰 전체를 조회합니다.

### HTTP Request

`GET rest/V1/reviews/mine`

### Response

JSON array([Review]($review-item))
