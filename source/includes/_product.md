# Product

## Get List

```shell
curl "lately.co.kr/rest/V1/lately/products?searchCriteria[pageSize]=10&customSort=rating_summary" \
  -H "Content-Type: application/json"
```

> 위의 명령어는 다음과 같은 구조의 JSON을 리턴합니다

``` json
{
  "items": [{...}, {...}],
  "search_criteria": {
    "filter_groups": [],
    "page_size": 10
  },
  "total_count": 2
}
```

조건에 맞는 제품 목록을 조회합니다.

### HTTP Request

`GET rest/V1/lately/products`

### Response

JSON [Product Search Results](#TODO)

### Query Parameters
Parameter | Default | Description
--------- | ------- | -----------
searchCriteria |  | 검색 조건들
customFilters |  | 검색할 Custom Attribute의 조건들
customSort | | 정렬 기준이 될 Custom Attribute

### 검색조건(searchCriteria)

Magento의 기본 검색 인터페이스로 속성에 따라 검색(filter), 정렬(sort)을 어떻게 할지 설정합니다.

> 아래의 예시는 상품을 최근에 등록한 순이고 동시에 등록된 경우에 이름순(ABC)로 정렬합니다.

``` json
{
    "searchCriteria": {
        "sortOrders": [{
            "field": "created_at",
            "direction": "desc"
        }, {
            "field": "name",
            "direction": "asc"
        }]
    }
}
```

- sortOrders: sortOrders는 `field`, `direction` 속성을 갖는 객체의 배열입니다.
direction은 asc, desc만 가능하고 이외의 값을 입력할 경우에 에러가 납니다. field는 정렬할 기준이
될 속성인데 정렬할 수 없는 속성이나 없는 속성을 입력해도 결과는 나오지만 정렬되지 않은 값이 나오므로
direction을 반대로해서 제대로 정렬이 되는지 확인하는 것이 좋습니다. 우선순위는 `customSort`가
먼저고 sortOrders에 입력한 순서대로입니다.

> 아래의 예시는 가격이 200원 이하고 sku에 red가 포함되는 상품을 검색합니다.
 
``` json
{
  "searchCriteria": {
    "pageSize": 10,
    "filterGroups": [{
      "filters": [{
        "field": "price",
        "value": 200,
      "condition_type": "lt"
      }]
    }, {
      "filters": [{
        "field": "sku",
        "value": "%red%",
        "condition_type": "like"
      }]
    }]
  }
}
```

- filterGroups: filterGroups는 `filters`의 배열이며 이 값들은 and 조건으로 검색됩니다. 그리고
filters는 다시 `field`, `value`, `condition_type`을 속성으로 갖는 객체의
배열이 되는데 이 값들은 or 조건으로 검색됩니다. field는 검색할 속성, value는 검색할 값이며,
condition_type은 생략할 경우 "eq"가 기본값이 되고, "lt", "gt", "in", "like"를 사용할 수
있습니다.

### Custom Attribute 검색(customFilters)

> 제조사 인덱스가 8이며 색상 인덱스가 5인 제품을 10개씩 검색합니다.

``` json
{
    "searchCriteria": { "pageSize": 10 },
    "customFilters": "manufacturer=8,color=5"
}
```

Magento에서 현재까지(4/27 기준) Custom Attribute에 대한 검색을 REST API로 지원하지 않아서
새로 만든 기능입니다. 그래서 searchCriteria보다 단순하고 사용법이 다릅니다. 제조사(manufacturer)의
인덱스가 8인 제품을 검색하고 싶으면 `manufacturer=8`, 그리고 색상이 red(예를 들어 인덱스가 5)인
제품을 검색하고 싶으면 `color=5`, 두 조건 모두 충족하는 제품은 `manufacturer=8,color=5`처럼
콤마(,)로 이어서 `customFilters`에 넘겨주면 됩니다. MD추천(featured) 상품인지에 대한 값은
True/False이며 Magento 내부에서 1/0으로 저장하고 있는데, True인 값을 검색할 때는 값을 생략하고
검색할 수 있습니다. 예를 들어 `color=5,featured,manufacturer=8`라면 위의 조건에서 MD추천인지를
추가한 세가지 조건을 모두 만족하는 제품을 검색하게 됩니다. 존재하지 않는 Custom Attribute일 때는
"Invalid attribute name"라는 에러가 발생합니다.

### Custom Attribute 정렬(customSort)

> 평점이 높은 순서대로 10개씩 검색합니다.

``` json
{
    "searchCriteria": { "pageSize": 10 },
    "customSort": "rating_summary"
}
```

Custom Attribute를 등록할 때 Storefront Properties에서 
"Used for Sorting in Product Listing"가 설정된 속성만 가능합니다. default value를 0으로
설정하지 않으면 속성값이 없는 제품부터 정렬됩니다.

### JSON to Form

위의 예제에서는 JSON의 형태로 되어있는데 실제로 URL parameter로 넘겨줄 때는 Form Data가
serialize된 형태로 넘겨줘야합니다. 간단히 [이곳](http://codepen.io/seoh/full/pyxEjy/)에서
변환할 수 있습니다.

