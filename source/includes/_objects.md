# Objects

API 에서 사용되는 객체들의 형식을 설명합니다.


## Chat Room

각 채팅방을 나타내는 객체

Name | Type | Description
---- | ---- | -----------
_id | string | ID of this chat room
userId | string | User ID
sellerId | string | seller user ID
createdTime | string | time of chat room created ISO 8601 date (2015-03-25T12:00:00)


## Chat Message

채팅 메세지

Name | Type | Description
---- | ---- | -----------
_id | string | ID of this message
userId | string | Sender User ID
message | string | message
createdTime | string | time of message sent ISO 8601 date (2015-03-25T12:00:00)


## Wishlist Object

찜 목록

Name | Type | Description
---- | ---- | -----------
wishlist_id  | int    | Wishlist ID
customer_id  | int    | Customer ID
shared       | int    | Sharing flag (0 or 1)
sharing_code | string | Sharing encrypted code
updated_at   | string | Last updated date
items | array([Wishlist Item](#wishlist-item)) | Wishlist items

## Wishlist Item

찜 항목

Name | Type | Description
---- | ---- | -----------
wishlist_item_id  | int    | Wishlist item ID
wishlist_id       | int    | Wishlist ID
store_id          | int    | Store ID
added_at          | string | Add date and time
description       | string | Short description of wish list item
qty               | int    | Qty
product_id        | int    | Product ID
sku                | string | 제품의 SKU
name               | string | 제품의 이름
price              | int    | 제품의 가격
manufacturer       | string | 제품 제조사
manufacturer_korea | string | 제품 제조사의 한글명
thumbnail          | string | 제품 사진의 썸네일


## Product Reviews

제품별 리뷰 목록

Name | Type | Description
---- | ---- | -----------
reviews_count   | int | 제품에 등록된 총 리뷰 개수
rating_summary  | int | 등록된 리뷰의 평점 (100분율)
reviews         | array([Review]($review-item)) | 등록된 리뷰 목록


## Review Item

리뷰 상세 정보

Name | Type | Description
---- | ---- | -----------
review_id | int | 리뷰 아이디
created_at | string | 리뷰를 등록한 시각
title | string | 리뷰의 제목
detail | string | 리뷰의 내용
nickname | string | 리뷰를 등록한 닉네임
customer_id | int | 리뷰를 등록한 사용자
rating | int | 리뷰에 포함된 평점
product_id | int | 리뷰를 남긴 제품의 아이디
sku | string | 리뷰를 남긴 제품의 SKU
manufacturer | string | 리뷰를 남긴 제품 제조사
manufacturer_korea | string | 리뷰를 남긴 제품 제조사의 한글명
thumbnail | string | 리뷰를 남긴 제품 사진의 썸네일

